#include "lifoda_jpeg_private.h"

enum {
	C1 = _LFD_JPEG_C1,
	C2 = _LFD_JPEG_C2,
	C3 = _LFD_JPEG_C3,
	C4 = _LFD_JPEG_C4,
	C5 = _LFD_JPEG_C5,
	C6 = _LFD_JPEG_C6,
	C7 = _LFD_JPEG_C7,
	A0 = _LFD_JPEG_A0,
	A1 = _LFD_JPEG_A1,
	A2 = _LFD_JPEG_A2,
	A3 = _LFD_JPEG_A3,
	H = _LFD_JPEG_H,
	B = _LFD_JPEG_B
};

typedef struct _lifoda_jpeg_decoder_extra_t {	
	lifoda_jpeg_patch_write_t *pWrite;
	lifoda_jpeg_read_t *pRead;
	void *pRContext;
	void *pWContext;

	jmp_buf jbEoi;

	u08_t *apQTable [4];

	u16_t *pDCLut [4];
	u16_t *pACLut [4];
	
	uint_t iWidth;
	uint_t iHeight;
	uint_t iChannels;

	uint_t iVScale [3];
	uint_t iHScale [3];
	uint_t iVMaxScale;
	uint_t iHMaxScale;

	u08_t *apChQTable [3];
	u16_t *apChDCLut [3];
	u16_t *apChACLut [3];
	u08_t aiCsj [3];
	
	u08_t *pBuffer;
	u08_t *pBufferEnd;	

	u32_t iShiftReg;
	int iValidBits;

} lifoda_jpeg_decoder_extra_t;

#define __DECODER_XTRA(X) ((lifoda_jpeg_decoder_extra_t *)((X)->pExtra))

static const u08_t __jpeg_izigzag [64] = {	
	0,  1,  8, 16,  9,  2,  3,  10, 
	17, 24, 32, 25, 18, 11,  4,  5,
	12, 19, 26, 33, 40, 48, 41, 34, 
	27, 20, 13,  6,  7, 14, 21, 28,
	35, 42, 49, 56, 57, 50, 43, 36, 
	29, 22, 15, 23, 30, 37, 44, 51,
	58, 59, 52, 45, 38, 31, 39, 46, 
	53, 60, 61, 54, 47, 55, 62, 63
} ; 

LFD_SMALL_PRIVATE u08_t __jpeg_u8clamp (iint_t const i) {
	return i <= 0x00 ? 0x00 : (i >= 255 ? 255 : (u08_t)i);
}

LFD_SMALL_PRIVATE i08_t __jpeg_i8clamp (iint_t const i) {
	return i <= -128 ? -128 : (i >= 127 ? 127 : (u08_t)i);
}

LFD_LARGE_PRIVATE void __jpeg_idct (iint_t * const in, i08_t * const o, uint_t c) {

	iint_t a0, a1, a2, a3, b0, b1, b2, b3;
	register uint_t i, j;

	for (i = 0; i < 64; i+=8) {				
		a0 = C4*(in [i] + in [i+4]) + H;
		a1 = C4*(in [i] - in [i+4]) + H;
		a2 = C6*in [i+6] + C2*in [i+2];
		a3 = C2*in [i+6] - C6*in [i+2];

		b0 = a0 + a2;
		b1 = a1 - a3;
		b2 = a1 + a3;										 
		b3 = a0 - a2;

		a0 = C1*in [i+1] + C3*in [i+3] + C5*in [i+5] + C7*in [i+7];
		a1 = C3*in [i+1] - C7*in [i+3] - C1*in [i+5] - C5*in [i+7];
		a2 = C5*in [i+1] - C1*in [i+3] + C7*in [i+5] + C3*in [i+7];
		a3 = C7*in [i+1] - C5*in [i+3] + C3*in [i+5] - C1*in [i+7];

		in [i+0] = (b0 + a0) >> B;
		in [i+1] = (b1 + a1) >> B;
		in [i+2] = (b2 + a2) >> B;
		in [i+3] = (b3 + a3) >> B;
		in [i+4] = (b3 - a3) >> B;
		in [i+5] = (b2 - a2) >> B;
		in [i+6] = (b1 - a1) >> B;
		in [i+7] = (b0 - a0) >> B;
	}

	for (i = 0; i < 8; ++i) {
		a0 = C4*(in [i] + in [i+32]) + H;
		a1 = C4*(in [i] - in [i+32]) + H;
		a2 = C6*in [i+48] + C2*in [i+16];
		a3 = C2*in [i+48] - C6*in [i+16];

		b0 = a0 + a2 ;
		b1 = a1 - a3 ;
		b2 = a1 + a3 ;										 
		b3 = a0 - a2 ;

		a0 = C1*in [i+8] + C3*in [i+24] + C5*in [i+40] + C7*in [i+56];
		a1 = C3*in [i+8] - C7*in [i+24] - C1*in [i+40] - C5*in [i+56];
		a2 = C5*in [i+8] - C1*in [i+24] + C7*in [i+40] + C3*in [i+56];
		a3 = C7*in [i+8] - C5*in [i+24] + C3*in [i+40] - C1*in [i+56];

		j = (i<<1) + i + c;

		o [j + 0x00] = __jpeg_i8clamp ((b0 + a0) >> B);
		o [j + 0x18] = __jpeg_i8clamp ((b1 + a1) >> B);
		o [j + 0x30] = __jpeg_i8clamp ((b2 + a2) >> B);
		o [j + 0x48] = __jpeg_i8clamp ((b3 + a3) >> B);
		o [j + 0x60] = __jpeg_i8clamp ((b3 - a3) >> B);
		o [j + 0x78] = __jpeg_i8clamp ((b2 - a2) >> B);
		o [j + 0x90] = __jpeg_i8clamp ((b1 - a1) >> B);
		o [j + 0xA8] = __jpeg_i8clamp ((b0 - a0) >> B);
	}
}

LFD_SMALL_PRIVATE void const *__jpeg_fetch (lifoda_jpeg_context_t * const pContext, void const **pData, uint_t const size) {
	if (__DECODER_XTRA (pContext)->pRead (__DECODER_XTRA (pContext)->pRContext, pData, size) < (int)size)
		__jpeg_exit (pContext, LFD_JPEG_ERROR_READ);
	return pData ? *pData : NULL;
}

LFD_SMALL_PRIVATE void const *__jpeg_fetch_return (lifoda_jpeg_context_t * const pContext, uint_t const iSize) {
	void const *pData;	
	return __jpeg_fetch (pContext, &pData, iSize);
}

#define __jpeg_fetch_array(pContext, T, N) \
	(T *)__jpeg_fetch_return (pContext, N*sizeof (T))

LFD_SMALL_PRIVATE u16_t __jpeg_fetch_imm (lifoda_jpeg_context_t * const pContext) {
	register u16_t const * const k = 
		__jpeg_fetch_return (pContext, sizeof (*k));
	return __jpeg_u16 (*k);
}


LFD_LARGE_PRIVATE void __jpeg_fetch_dqt (lifoda_jpeg_context_t * const pContext, uint_t length) {
	u08_t const * data;
	data = __jpeg_fetch_return (pContext, length);

	__jpeg_assert (pContext, LFD_JPEG_ERROR_BAD_FORMAT, length == 65 && *data < 4);

	*(__DECODER_XTRA (pContext)->apQTable + *data) = 
		__jpeg_alloc_and_copy (pContext, data + 1, 64);
}

LFD_SMALL_PRIVATE uint_t __jpeg_u8sum (u08_t const * const w, uint_t const n) {
	register uint_t i,s ;
	for (s = i = 0; i < n; ++i)
		s = s + w [i];
	return s;
}

LFD_SMALL_PRIVATE u08_t *__jpeg_build_sizes (u08_t * const s, u08_t const * const w, uint_t const n) {
	register uint_t i, j, k;
	for (k = 0, i = 0; i < 16; ++i)
		for (j = 0; j < w [i]; ++j, ++k)
			s [k] = (i + 1) & 0xff;
	return s;
}

LFD_SMALL_PRIVATE u16_t *__jpeg_build_codes (u16_t * const h, u08_t const * const s, uint_t const n) {
	register uint_t i, c;
	for (i = c = 0; i < n; c += (0x10000 >> s [i]), ++i)
		h [i] = c & 0xffff;
	return h;
}

LFD_LARGE_PRIVATE u16_t *__jpeg_build_tables (
	lifoda_jpeg_context_t *pContext, 
	u16_t const * const H, 
	u08_t const * const S, 
	u08_t const * const V,
	usize_t const N)
{
	u16_t *pr, *pe;
	usize_t i = 0, j;
	u32_t lsb, msb, xbs;
	u16_t lsl, msl;

	pr = (u16_t *)__jpeg_alloc_and_zerro (pContext, 0x100*sizeof (*pr));

	while (S [i] <= 8) {
		xbs = (1u << (8 - S [i]));
		msb = (H [i] >> 8);
		msl = (S [i] << 8) | (V [i]);
		for (j = 0; j < xbs; ++j)
			pr [msb + j] = msl;
		if (++i >= N)
			return pr;
	}

	while (S [i] <= 16) {

		xbs = 1u << (16 - S [i]);
		lsb = H [i] & 0xff;
		msb = (H [i] >> 8);
		lsl = (S [i] << 8) | (V [i]);

#if defined(LFD_JPEG_DEBUG)
		if ((pr [msb] & 0xFF00) && !(pr [msb] & 0x8000)) {			
			__debugbreak ();
		}
#endif		

		if (pr [msb] & 0x8000) {
			pe = &pr [pr [msb] & 0x7FFF];
		}
		else {
			pe = (u16_t *)__jpeg_alloc_and_zerro (pContext, 256*sizeof (*pe));			
			pr [msb] = (0x8000 | (pe - pr)) & 0xFFFF;
		}

		for (j = 0; j < xbs; ++j)
			pe [lsb + j] = lsl;

		if (++i >= N)
			return pr;
	}

	while (i < N) {
		++i;
	}

	return pr;

}

LFD_LARGE_PRIVATE void __jpeg_fetch_dht (lifoda_jpeg_context_t * const pContext, uint_t const length) {
	uint_t tclass, tident, total, toff = 0;
	u08_t *hsize, *hval;
	u16_t *hcode, *hlut;	
	u08_t *temp;

	temp = __jpeg_fetch_array (pContext, u08_t, length);

	tclass = *temp >> 4;
	tident = *temp & 0xf;
	
	temp = temp + 1;

	__jpeg_assert (pContext, LFD_JPEG_ERROR_BAD_FORMAT, tclass <= 1 || tident <= 1);

	total = __jpeg_u8sum (temp, 16); 

	hval = __jpeg_new_and_copy (pContext, u08_t, temp + 16, total);

	hsize = __jpeg_build_sizes (__jpeg_top_new (pContext, u08_t, total), temp, 16);
	hcode = __jpeg_build_codes (__jpeg_top_new (pContext, u16_t, total), hsize, total);

	hlut = __jpeg_build_tables (pContext, hcode, hsize, hval, total);
	
	*(tclass ?
		&__DECODER_XTRA (pContext)->pACLut [tident] :
		&__DECODER_XTRA (pContext)->pDCLut [tident]) =
		hlut;

	__jpeg_heap_reset_top (pContext);

}

LFD_LARGE_PRIVATE void __jpeg_fetch_sof (lifoda_jpeg_context_t * const pContext, usize_t const length) {
	lifoda_jpeg_decoder_extra_t * const pXtra  = __DECODER_XTRA (pContext);
#pragma pack(push, 1)
	struct _sof {
		u08_t iPrecision;
		u16_t iHeight;
		u16_t iWidth;
		u08_t iChannels;
		struct _chan {
			u08_t iChannel;
			u08_t iHVScale;
			u08_t iQTable;
		} aChannels [] ;
	} * sof;
#pragma pack(pop)

	struct _chan *pChan;
	register uint_t i, cid;

	sof = (struct _sof *)__jpeg_fetch_return (pContext, length);

	__jpeg_assert (pContext, 
		LFD_JPEG_ERROR_BAD_FORMAT, 
		sof->iPrecision == 8 ||
		sof->iChannels == 1 || 
		sof->iChannels == 3);

	pXtra->iWidth = __jpeg_u16 (sof->iWidth);
	pXtra->iHeight = __jpeg_u16 (sof->iHeight);
	pXtra->iChannels = sof->iChannels;
	
	for (i = 0; i < pXtra->iChannels; ++i) {
		pChan = &sof->aChannels [i];
		cid = pChan->iChannel - 1;
		
		__jpeg_assert (pContext, 
			LFD_JPEG_ERROR_BAD_FORMAT, 
			cid < 3);

		__jpeg_assert (pContext, 
			LFD_JPEG_ERROR_BAD_FORMAT, 
			pChan->iQTable < 4);

		pXtra->iHScale [cid] = pChan->iHVScale >> 4;
		pXtra->iVScale [cid] = pChan->iHVScale & 0xf;
		pXtra->apChQTable [cid] = pXtra->apQTable [pChan->iQTable];

		__jpeg_assert (pContext, 
			LFD_JPEG_ERROR_BAD_FORMAT, 
			pXtra->iHScale [cid] <= 4 && 
			pXtra->iVScale [cid] <= 4);
																			  
		pXtra->iHMaxScale = __jpeg_umax (
			pXtra->iHMaxScale, 
			pXtra->iHScale [cid]);

		pXtra->iVMaxScale = __jpeg_umax (
			pXtra->iVMaxScale, 
			pXtra->iVScale [cid]);
	}	

}

LFD_LARGE_PRIVATE void __jpeg_fetch_sos (lifoda_jpeg_context_t * const pContext, usize_t const length) {
	lifoda_jpeg_decoder_extra_t * const pXtra = __DECODER_XTRA (pContext);
#pragma pack(push, 1)
	struct _sos_a {
		u08_t ns;
		struct _chan { u08_t csj, tdta; } apChannel [];
	} *sos_a;

	struct _sos_b {
		u08_t ss, se, ahal;
	} *sos_b;
#pragma pack(pop)
	register uint_t i, j, td, ta, s;
	u08_t *pSos;
	
	pSos = __jpeg_fetch_array (pContext, u08_t, length);

	sos_a = (struct _sos_a *)(pSos);
	sos_b = (struct _sos_b *)(pSos + 1 + 2*sos_a->ns);

	__jpeg_assert (pContext, 
		LFD_JPEG_ERROR_BAD_FORMAT, 
		sos_b->ss == 0 && 
		sos_b->se == 63 && 
		sos_b->ahal == 0);

	for (s = i = 0; i < sos_a->ns; ++i) {
		j = sos_a->apChannel [i].csj - 1; 		
		td = sos_a->apChannel [i].tdta ;

		ta = td & 0xf;
		td = td >> 4;

		__jpeg_assert (pContext, 
			LFD_JPEG_ERROR_BAD_FORMAT, 
			j < 3 && ta <= 1 && td <= 1);

		pXtra->apChACLut [j] = pXtra->pACLut [ta];
		pXtra->apChDCLut [j] = pXtra->pDCLut [ta];
		pXtra->aiCsj [i] = j;

		s = s + pXtra->iHScale [j] * pXtra->iVScale [j];
	}

	__jpeg_assert (pContext, 
		LFD_JPEG_ERROR_BAD_FORMAT, 
		s <= 10);
}

LFD_SMALL_PRIVATE iint_t __jpeg_buffer_check (lifoda_jpeg_context_t * const pContext, lifoda_jpeg_decoder_extra_t * const pXtra) {	
	uint_t s;
	if (pXtra->pBuffer < pXtra->pBufferEnd) 
		return 0;	
	s = pXtra->pRead (pXtra->pRContext, &pXtra->pBuffer, _LFD_JPEG_PREBUFFER);	
	if (s < 1) {
		__jpeg_exit (pContext, LFD_JPEG_ERROR_BAD_FORMAT);
		return -1; 
	}
	pXtra->pBufferEnd = pXtra->pBuffer + s;
	return 1;
}

LFD_SMALL_PRIVATE u32_t __jpeg_prebuffer (lifoda_jpeg_context_t * const pContext, uint_t const N, iint_t const iXtract) {
	lifoda_jpeg_decoder_extra_t * const pXtra = 
		__DECODER_XTRA (pContext);
	register uint_t b, sr, vb;

	sr = pXtra->iShiftReg;
	vb = pXtra->iValidBits;

	while (vb < N) {
		__jpeg_buffer_check (pContext, pXtra);
		b = *(pXtra->pBuffer++) ;		
		sr = (sr << 8) | b;
		vb += 8;
		if (b != 0xff)
			continue;		
		__jpeg_buffer_check (pContext, pXtra);
		b = *(pXtra->pBuffer++);
		if (b) longjmp (pXtra->jbEoi, b);
	}	

	pXtra->iShiftReg = sr;
	pXtra->iValidBits = iXtract ? vb - N : vb;

	return sr >> (vb - N);
}

LFD_SMALL_PRIVATE u32_t __jpeg_decode_char (u16_t const *pLutRoot, u16_t const code) {
	register u32_t msw = pLutRoot [code >> 8];
	return msw & 0x8000 ? pLutRoot [(msw & 0x7fff) + (code & 0xff)] : msw;
}


LFD_SMALL_PRIVATE iint_t __jpeg_extend (u32_t const d, u32_t const b) {
	return (iint_t)d & (1 << (b - 1)) ? d : (d|(0xffffffff << b)) + 1;
}

LFD_LARGE_PRIVATE void __jpeg_decode_block (
	lifoda_jpeg_context_t * const pContext,
	iint_t * const pBlock,
	iint_t * const dc,
	u16_t const * const pDCLut,
	u16_t const * const pACLut,
	u08_t const * const pQTable) {
	lifoda_jpeg_decoder_extra_t * const pXtra =
		__DECODER_XTRA (pContext);	
	u32_t bits, size, rept;
	iint_t bxtr;
	iint_t i = 1;

	if (setjmp (pXtra->jbEoi))
		return;

	memset (pBlock, 0, 64*sizeof (iint_t));

	bits = __jpeg_prebuffer (pContext, 16, 0) ;
	bits = __jpeg_decode_char (pDCLut, bits & 0xffff);

	size = bits >> 8;
	bits = bits & 0xff;

	bxtr = __jpeg_prebuffer (pContext, size + bits, 1);
	bxtr = bits > 0 ? __jpeg_extend (bxtr & ((1 << bits) - 1), bits) : 0;

	*dc = pBlock [0] = *dc + bxtr;
	pBlock [0] *= pQTable [0];

	while (i < 64) {
		bits = __jpeg_prebuffer (pContext, 16, 0) ;
		bits = __jpeg_decode_char (pACLut, bits & 0xffff);

		size = bits >> 8;
		bits = bits & 0xff;
		rept = bits >> 4;
		bits = bits & 0xf;

		bxtr = __jpeg_prebuffer (pContext, size + bits, 1);
		bxtr = bits > 0 ? __jpeg_extend (bxtr & ((1 << bits) - 1), bits) : 0;

		if (bits > 0) {
			i = i + rept;
			pBlock [__jpeg_izigzag [i]] =
				pQTable [i] * bxtr;
			++i;
			continue;
		}

		if (rept < 0xf)
			break;		
		i = i + 16;
		continue;

	}

}

LFD_SMALL_PRIVATE void __jpeg_yuv24_to_rgb24 (i08_t * const pBuffer) {

}

LFD_LARGE_PRIVATE void __jpeg_decode_scan (lifoda_jpeg_context_t * const pContext) {
	lifoda_jpeg_decoder_extra_t * const pXtra = 
		__DECODER_XTRA (pContext);
	lifoda_jpeg_rect_t rect;

	iint_t dc [3] = {0, 0, 0};

	u08_t *pQTable [3];
	u16_t *pACLut [3];
	u16_t *pDCLut [3];

	iint_t *pBlockIn;	
	i08_t *pPixels;
	iint_t y, u, v;
	uint_t i;

	rect.x = 0;
	rect.y = 0;
	rect.w = pXtra->iWidth;
	rect.h = pXtra->iHeight;

	pXtra->pWrite (pXtra->pWContext, NULL, &rect, LFD_JPEG_MODE_RGB24);
	
	rect.w = 8;
	rect.h = 8;

	pBlockIn = __jpeg_top_new (pContext, iint_t, rect.w*rect.h);	
	pPixels = __jpeg_top_new (pContext, u08_t, rect.w*rect.h*3);
	
	for (i = 0; i < pXtra->iChannels; ++i) {
		pDCLut [i] = pXtra->apChDCLut [pXtra->aiCsj [i]];
		pACLut [i] = pXtra->apChACLut [pXtra->aiCsj [i]];
		pQTable [i] = pXtra->apChQTable [pXtra->aiCsj [i]];
	}

	for (rect.y = 0; rect.y < pXtra->iHeight; rect.y += rect.h) {
		for (rect.x = 0; rect.x < pXtra->iWidth; rect.x += rect.w) {

			memset (pPixels, 0, rect.w*rect.h*3);
			for (i = 0; i < pXtra->iChannels; ++i) {				
				__jpeg_decode_block (pContext, pBlockIn, &dc [i], pDCLut [i], pACLut [i], pQTable [i]);
				__jpeg_idct (pBlockIn, pPixels, i);				
			}

			for (i = 0; i < 192; i += 3) {
				y = pPixels [i+0];
				u = pPixels [i+1];
				v = pPixels [i+2];
				y = ((y + 0x80) << B) + H;
				pPixels [i+0] = __jpeg_u8clamp ((y + A0*v) >> B);
				pPixels [i+2] = __jpeg_u8clamp ((y + A3*u) >> B);
				pPixels [i+1] = __jpeg_u8clamp ((y - A1*u - A2*v) >> B);
			}

			if (!pXtra->pWrite (pXtra->pWContext, pPixels, &rect, LFD_JPEG_MODE_RGB24)) 
				__jpeg_exit (pContext, LFD_JPEG_ERROR_ABORT);
			
		}
	}

}

/**
 *	EXTERNAL FUNCTIONS
 */

int lifoda_jpeg_decode (
	lifoda_jpeg_read_t *pRead,
	void *pRContext,
	lifoda_jpeg_patch_write_t *pWrite,
	void *pWContext,
	void *pHeap,
	usize_t iHeapSize,
	u32_t iFlags)
{
	u16_t marker = 0;
	u16_t length = 0;

	lifoda_jpeg_context_t context;
	lifoda_jpeg_decoder_extra_t *pXtra;

	memset (&context, 0, sizeof (context));

	if (__jpeg_enter (&context)) {
		return context.iError;
	}

	if (!iHeapSize && pHeap && !(iHeapSize & LFD_JPEG_USE_MALLOC)) {
		__jpeg_exit (&context, LFD_JPEG_ERROR_INVALID_ARGS);
	}

	iHeapSize = iHeapSize > 0 ? iHeapSize : 256 * 1024;

	if (!pHeap || (iFlags & LFD_JPEG_USE_MALLOC)) {
		pHeap = malloc (iHeapSize);
		iFlags |= LFD_JPEG_USE_MALLOC;
	}

	context.pHeapStart = (u08_t *)pHeap;
	context.pHeapEnd = (u08_t *)pHeap + iHeapSize;
	context.iHeapSize = iHeapSize;
	context.iFlags = iFlags;

	pXtra = __jpeg_new_and_zerro (&context, lifoda_jpeg_decoder_extra_t, 1);

	context.pExtra = pXtra;

	pXtra->pRContext = pRContext;
	pXtra->pWContext = pWContext;
	pXtra->pRead = pRead;
	pXtra->pWrite = pWrite;

	for (;;) {		
		length = 0;
		marker = __jpeg_fetch_imm (&context);
		if (marker < 0xff00) 
			__jpeg_exit (&context, LFD_JPEG_ERROR_BAD_FORMAT);		
		marker = marker & 0xff;		
		if (marker < 0xD0 || marker >= 0xDA)
			length = __jpeg_fetch_imm (&context) - sizeof (length);
		switch (marker) {

		case 0xDB: // Define quantization tables
				__jpeg_fetch_dqt (&context, length);
				continue;

			case 0xC4: // Define huffman table
				__jpeg_fetch_dht (&context, length);
				continue;

			case 0xC0: // Start of frame (Baseline DCT)
				__jpeg_fetch_sof (&context, length);
				continue;

			case 0xDA: // Start of scan data								
				__jpeg_fetch_sos (&context, length);					
				__jpeg_decode_scan (&context);

				return LFD_JPEG_SUCCESS;

			case 0xD8: // Sart of image
				continue;

			case 0xD9: // End of image
				__jpeg_exit (&context, 0);
				continue;

			case 0xFE: // Comment
			case 0xE0: // JFIF/JFXX header
			default:
				__jpeg_fetch (&context, NULL, length);		// skip
				continue;

			case 0xC1: // Exteded sequential DCT (Huffman)
			case 0xC2: // Progressive DCT (Huffman)
			case 0xC3: // Lossless (Huffman)
			case 0xC5: // Differential sequential DCT (Huffman)
			case 0xC6: // Differential progressive DCT (Huffman)
			case 0xC7: // Differential lossless (Huffman)
			case 0xC8: // Reserved for extensions (Arithmetic)
			case 0xC9: // Extended sequential DCT (Arithmetic)
			case 0xCA: // Progressive DCT (Arithmetic)
			case 0xCB: // Lossless sequential (Aritmetic)
			case 0xCD: // Differential sequential DCT (Arithmetic)
			case 0xCE: // Differential progressive DCT (Arithmetic)
			case 0xCF: // Differential lossless sequential (Arithmetic)
			case 0xCC: // Define arithmetic coding conditions
				__jpeg_exit (&context, LFD_JPEG_ERROR_BAD_FORMAT);
				continue;
		}

	}

	return LFD_JPEG_SUCCESS;
}

