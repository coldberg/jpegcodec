#ifndef __LIFODA_JPEG_PRIVATE_H__
#define __LIFODA_JPEG_PRIVATE_H__ 

#define _LFD_JPEG_B  16
#define _LFD_JPEG_H  32768					// 2^(B - 1)
#define _LFD_JPEG_C1 32138					// cos (1*PI/16)/2
#define _LFD_JPEG_C2 30274					// cos (2*PI/16)/2
#define _LFD_JPEG_C3 27246					// cos (3*PI/16)/2		
#define _LFD_JPEG_C4 23170					// cos (4*PI/16)/2, cos (0*PI/16)/(2*sqrt(2)) 									 
#define _LFD_JPEG_C5 18205					// cos (5*PI/16)/2
#define _LFD_JPEG_C6 12540					// cos (6*PI/16)/2
#define _LFD_JPEG_C7 6393					// cos (7*PI/16)/2

#define _LFD_JPEG_A0 91881
#define _LFD_JPEG_A1 22553
#define _LFD_JPEG_A2 46801 
#define _LFD_JPEG_A3 116129

#define _LFD_JPEG_PREBUFFER 1024

#define _USE_SSE

#ifdef _MSC_VER 
#define LFD_LARGE_PRIVATE	static
#define LFD_SMALL_PRIVATE	static /* __inline */
#define LFD_PUBLIC
#else
#define LDF_LARGE_PRIVATE	static
#define LDF_SMALL_PRIVATE	static __inline__ 
#define LFD_PUBLIC 
#endif

#define _USE_MATH_DEFINES

#include "lifoda_jpeg.h"
#include <stddef.h>
#include <setjmp.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <math.h>
#include <intrin.h>


typedef struct _lifoda_jpeg_context_t {
	u08_t		*pHeapStart;
	u08_t		*pHeapEnd;
	usize_t		iHeapSize;
	usize_t		iHeapUsed; 
	usize_t		iHeapTopUsed;
	usize_t		iMaxHeapUsed;
	u32_t		iFlags;
	int			iError;	
	jmp_buf		jbError;
	void		*pExtra;
} lifoda_jpeg_context_t;

LFD_SMALL_PRIVATE u16_t __jpeg_u16 (u16_t const w) {
	return (w << 8) | (w >> 8);
}

LFD_SMALL_PRIVATE u08_t __jpeg_log2 (int const val) {
	register i32_t i = 0, v = val < 0 ? -val : val;
	while (v) v >>= 1, ++i;
	return i & 0xff;
}

LFD_SMALL_PRIVATE usize_t __jpeg_umax (usize_t const a, usize_t const b) {
	return a > b ? a : b;	
}

LFD_SMALL_PRIVATE void __jpeg_exit (lifoda_jpeg_context_t * const pContext, int const iError) {
#if defined(LFD_JPEG_DEBUG)
	if (iError) __debugbreak ();
#endif
	pContext->iError = iError;
	longjmp (pContext->jbError, iError);
}

LFD_SMALL_PRIVATE int __jpeg_enter (lifoda_jpeg_context_t * const pContext) {
	pContext->iError = LFD_JPEG_SUCCESS;	
	return setjmp (pContext->jbError); 
}

LFD_SMALL_PRIVATE u32_t __jpeg_heap_free (lifoda_jpeg_context_t * const pContext) {
	return pContext->iHeapSize - pContext->iHeapUsed - pContext->iHeapTopUsed;
}

LFD_SMALL_PRIVATE void __jpeg_assert (lifoda_jpeg_context_t * const pContext, int iError, int iCond) {
	if (iCond) return;
	__jpeg_exit (pContext, iError);
}

LFD_SMALL_PRIVATE void __jpeg_heap_assert (lifoda_jpeg_context_t * const pContext, usize_t const iSize) {	
	__jpeg_assert (pContext, LFD_JPEG_ERROR_OUT_OF_MEMORY, iSize <= __jpeg_heap_free (pContext));
}

LFD_SMALL_PRIVATE void *__jpeg_alloc (lifoda_jpeg_context_t *const pContext, usize_t const iSize) {
	register void *pHeap = NULL;
	__jpeg_heap_assert (pContext, iSize);
	pHeap = pContext->pHeapStart + pContext->iHeapUsed;
	pContext->iHeapUsed += iSize;
#if defined(LFD_JPEG_DEBUG)
	pContext->iMaxHeapUsed = __jpeg_umax (pContext->iMaxHeapUsed, pContext->iHeapUsed + pContext->iHeapTopUsed);
#endif
	return pHeap;
}

LFD_SMALL_PRIVATE void *__jpeg_alloc_and_copy (lifoda_jpeg_context_t *const pContext, void const *pSource, usize_t const iSize) {
	register void *pHeap = __jpeg_alloc (pContext, iSize);
	memcpy (pHeap, pSource, iSize);
	return pHeap;
}

LFD_SMALL_PRIVATE void *__jpeg_alloc_and_zerro (lifoda_jpeg_context_t *const pContext, usize_t const iSize) {
	register void *pHeap = __jpeg_alloc (pContext, iSize);
	memset (pHeap, 0, iSize);
	return pHeap;
}

LFD_SMALL_PRIVATE void *__jpeg_alloc_top (lifoda_jpeg_context_t *const pContext, usize_t iSize) {
	__jpeg_heap_assert (pContext, iSize);
	pContext->iHeapTopUsed += iSize;
#if defined(LFD_JPEG_DEBUG)
	pContext->iMaxHeapUsed = __jpeg_umax (pContext->iMaxHeapUsed, pContext->iHeapUsed + pContext->iHeapTopUsed);
#endif
	return pContext->pHeapEnd - pContext->iHeapTopUsed;
}

LFD_SMALL_PRIVATE void __jpeg_heap_reset_top (lifoda_jpeg_context_t *const pContext) {
	pContext->iHeapTopUsed = 0;
}

#define __jpeg_top_new(pContext, T, N) \
	(T *)__jpeg_alloc_top (pContext, N*sizeof (T))

#define __jpeg_new(pContext, T, N)	\
	(T *)__jpeg_alloc (pContext, N*sizeof (T))

#define __jpeg_new_and_zerro(pContext, T, N) \
	(T *)__jpeg_alloc_and_zerro (pContext, N*sizeof (T))

#define __jpeg_new_and_copy(pContext, T, S, N) \
	(T *)__jpeg_alloc_and_copy (pContext, S, N*sizeof (T))


LFD_SMALL_PRIVATE void __jpeg_rollback (lifoda_jpeg_context_t *const pContext, usize_t const iSize) {
	__jpeg_assert (pContext, pContext->iHeapUsed >= iSize, LFD_JPEG_ERROR_OUT_OF_MEMORY);
	pContext->iHeapUsed -= iSize;
}
#if defined(LFD_JPEG_DEBUG)

#define __jpeg_printf(...) printf (__VA_ARGS__)
#define _LFD_DEBUG_BLOCK(...) __VA_ARGS__

LFD_SMALL_PRIVATE void pbin (u16_t b, u08_t s) {
	for (int i = 0; i < s; ++i) {
		__jpeg_printf ("%c", b & 0x8000 ? '1' : '0');
		b <<= 1;
	}
}

#else 

#define __jpeg_printf(...)
#define _LFD_DEBUG_BLOCK(...)
#define pbin(b,s)

#endif




#endif
