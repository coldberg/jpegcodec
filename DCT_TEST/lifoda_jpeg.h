#ifndef __LIFODA_JPEG_H__
#define __LIFODA_JPEG_H__

typedef   signed char							i08_t;
typedef   signed short							i16_t;
typedef   signed long							i32_t, isize_t, ioff_t;
typedef   signed __int64						i64_t;
typedef	  signed int							iint_t;

typedef unsigned char							u08_t;
typedef unsigned short							u16_t;
typedef unsigned long							u32_t, usize_t, uoff_t;
typedef unsigned __int64						u64_t;
typedef	unsigned int							uint_t;

#define LFD_JPEG_MODE_RGB24						0
#define LFD_JPEG_USE_MALLOC						1

#define LFD_JPEG_SUCCESS						0
#define LFD_JPEG_ERROR_ABORT				   -1		// User aborted
#define LFD_JPEG_ERROR_READ					   -2		// Fetch error
#define LFD_JPEG_ERROR_WRITE				   -3		// Write error
#define LFD_JPEG_ERROR_OUT_OF_MEMORY		   -4		// Out of memory
#define LFD_JPEG_ERROR_INVALID_ARGS			   -5		// Invalid args passed
#define LFD_JPEG_ERROR_BAD_FORMAT			   -6		// Unsupported JPEG format or error in file

typedef struct _lifoda_jpeg_rect_t {
	u32_t x, y, w, h;									// X, Y, Width and Height
} lifoda_jpeg_rect_t;

typedef int lifoda_jpeg_read_t (						// read from encoded stream callback
	void *pContext,										// the context specified when initializing the decoder
	void const **pBuffer,								// return a pointer to buffered data
	usize_t size);										// number of bytes to fetch

typedef int lifoda_jpeg_write_t (						// write to encoded stream callback 
	void *pContext,										// the context specified when initializing the encoder
	void const *pBuffer, 								// buffer from which to write the data, if this is NULL skip don't actually write, just seek to the right location
	usize_t size);										// number of bytes to write

typedef int lifoda_jpeg_patch_read_t (					// Fetch 8x8 or bigger patch of a bitmap
	void *pContext,										// the bitmap context passed to the encoder function
	u08_t const **pBuffer,								// return a pointer with buffered data
														// of the bitmap in pRect (x = 0, y = 0, w = width, h = height) in pixels
	lifoda_jpeg_rect_t *pRect,							// Rectangle to fetch, pad if nescassary
	int flags);											// Format and flags of the data to be retriedved

typedef int lifoda_jpeg_patch_write_t (					// Output 8x8 or bigger patch of a bitmap
	void *pContext,										// The bitmap context passed to the decoder
	u08_t const *pBuffer,								// Buffer of the data to write , if this is NULL you will receive the 
														// total size of the bitmap to be decoded , you must allocate a buffer 
														// if nescassary and make any other preparations
	lifoda_jpeg_rect_t const *pRect,					// Rectangle size given, might need clipping
	int flags);											// Format and flags of the data to be written

#ifdef __cplusplus
extern "C" {
#endif


int lifoda_jpeg_decode (
	lifoda_jpeg_read_t *pRead,
	void *pRContext,
	lifoda_jpeg_patch_write_t *pWrite,
	void *pWContext,
	void *pHeap,
	usize_t iHeapSize,
	u32_t iFlags);

int lifoda_jpeg_encode (
	lifoda_jpeg_patch_read_t *pRead,
	void *pRContext,
	lifoda_jpeg_write_t *pWrite,
	void *pWContext,
	void *pHeap,
	usize_t iHeapSize,
	u32_t iFlags);

#ifdef __cplusplus
}
#endif


#endif

