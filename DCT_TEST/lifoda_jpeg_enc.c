#include "lifoda_jpeg.h"
#include "lifoda_jpeg_private.h"

#include <malloc.h>
#include <string.h>

#if 0

static const u08_t __jpeg_fzigzag[64] = {
	0, 1, 5, 6, 14, 15, 27, 28,
	2, 4, 7, 13, 16, 26, 29, 42,
	3, 8, 12, 17, 25, 30, 41, 43,
	9, 11, 18, 24, 31, 40, 44, 53,
	10, 19, 23, 32, 39, 45, 52, 54,
	20, 22, 33, 38, 46, 51, 55, 60,
	21, 34, 37, 47, 50, 56, 59, 61,
	35, 36, 48, 49, 57, 58, 62, 63
};

static const u08_t __jpeg_y_dc_size[16] = {
	2, 3, 3, 3, 3, 3, 4, 5, 6, 7, 8, 9, 0, 0, 0, 0

};

static const u16_t __jpeg_y_dc_code[16] = {
	0x0000, 0x0002, 0x0003, 0x0004, 0x0005, 0x0006, 0x000E, 0x001E, 0x003E, 0x007E, 0x00FE, 0x01FE, 0x0000, 0x0000, 0x0000, 0x0000
};

static const u08_t __jpeg_y_ac_size[256] = {
	4, 2, 2, 3, 4, 5, 7, 8, 10, 16, 16, 0, 0, 0, 0, 0,
	0, 4, 5, 7, 9, 11, 16, 16, 16, 16, 16, 0, 0, 0, 0, 0,
	0, 5, 8, 10, 12, 16, 16, 16, 16, 16, 16, 0, 0, 0, 0, 0,
	0, 6, 9, 12, 16, 16, 16, 16, 16, 16, 16, 0, 0, 0, 0, 0,
	0, 6, 10, 16, 16, 16, 16, 16, 16, 16, 16, 0, 0, 0, 0, 0,
	0, 7, 11, 16, 16, 16, 16, 16, 16, 16, 16, 0, 0, 0, 0, 0,
	0, 7, 12, 16, 16, 16, 16, 16, 16, 16, 16, 0, 0, 0, 0, 0,
	0, 8, 12, 16, 16, 16, 16, 16, 16, 16, 16, 0, 0, 0, 0, 0,
	0, 9, 15, 16, 16, 16, 16, 16, 16, 16, 16, 0, 0, 0, 0, 0,
	0, 9, 16, 16, 16, 16, 16, 16, 16, 16, 16, 0, 0, 0, 0, 0,
	0, 9, 16, 16, 16, 16, 16, 16, 16, 16, 16, 0, 0, 0, 0, 0,
	0, 10, 16, 16, 16, 16, 16, 16, 16, 16, 16, 0, 0, 0, 0, 0,
	0, 10, 16, 16, 16, 16, 16, 16, 16, 16, 16, 0, 0, 0, 0, 0,
	0, 11, 16, 16, 16, 16, 16, 16, 16, 16, 16, 0, 0, 0, 0, 0,
	0, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 0, 0, 0, 0, 0,
	11, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 0, 0, 0, 0, 0
};

static const u16_t __jpeg_y_ac_code[256] = {
	0x000A, 0x0000, 0x0001, 0x0004, 0x000B, 0x001A, 0x0078, 0x00F8, 0x03F6, 0xFF82, 0xFF83, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
	0x0000, 0x000C, 0x001B, 0x0079, 0x01F6, 0x07F6, 0xFF84, 0xFF85, 0xFF86, 0xFF87, 0xFF88, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
	0x0000, 0x001C, 0x00F9, 0x03F7, 0x0FF4, 0xFF89, 0xFF8A, 0xFF8B, 0xFF8C, 0xFF8D, 0xFF8E, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
	0x0000, 0x003A, 0x01F7, 0x0FF5, 0xFF8F, 0xFF90, 0xFF91, 0xFF92, 0xFF93, 0xFF94, 0xFF95, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
	0x0000, 0x003B, 0x03F8, 0xFF96, 0xFF97, 0xFF98, 0xFF99, 0xFF9A, 0xFF9B, 0xFF9C, 0xFF9D, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
	0x0000, 0x007A, 0x07F7, 0xFF9E, 0xFF9F, 0xFFA0, 0xFFA1, 0xFFA2, 0xFFA3, 0xFFA4, 0xFFA5, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
	0x0000, 0x007B, 0x0FF6, 0xFFA6, 0xFFA7, 0xFFA8, 0xFFA9, 0xFFAA, 0xFFAB, 0xFFAC, 0xFFAD, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
	0x0000, 0x00FA, 0x0FF7, 0xFFAE, 0xFFAF, 0xFFB0, 0xFFB1, 0xFFB2, 0xFFB3, 0xFFB4, 0xFFB5, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
	0x0000, 0x01F8, 0x7FC0, 0xFFB6, 0xFFB7, 0xFFB8, 0xFFB9, 0xFFBA, 0xFFBB, 0xFFBC, 0xFFBD, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
	0x0000, 0x01F9, 0xFFBE, 0xFFBF, 0xFFC0, 0xFFC1, 0xFFC2, 0xFFC3, 0xFFC4, 0xFFC5, 0xFFC6, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
	0x0000, 0x01FA, 0xFFC7, 0xFFC8, 0xFFC9, 0xFFCA, 0xFFCB, 0xFFCC, 0xFFCD, 0xFFCE, 0xFFCF, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
	0x0000, 0x03F9, 0xFFD0, 0xFFD1, 0xFFD2, 0xFFD3, 0xFFD4, 0xFFD5, 0xFFD6, 0xFFD7, 0xFFD8, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
	0x0000, 0x03FA, 0xFFD9, 0xFFDA, 0xFFDB, 0xFFDC, 0xFFDD, 0xFFDE, 0xFFDF, 0xFFE0, 0xFFE1, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
	0x0000, 0x07F8, 0xFFE2, 0xFFE3, 0xFFE4, 0xFFE5, 0xFFE6, 0xFFE7, 0xFFE8, 0xFFE9, 0xFFEA, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
	0x0000, 0xFFEB, 0xFFEC, 0xFFED, 0xFFEE, 0xFFEF, 0xFFF0, 0xFFF1, 0xFFF2, 0xFFF3, 0xFFF4, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
	0x07F9, 0xFFF5, 0xFFF6, 0xFFF7, 0xFFF8, 0xFFF9, 0xFFFA, 0xFFFB, 0xFFFC, 0xFFFD, 0xFFFE, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000
};

// Prebuilt dht for DC values
static const u08_t __jpeg_y_dc_dht[] = {
	0xFF, 0xC4,	 // DHT marker
	0x00, 0x1F,  // Length
	0x00,		 // Class = 0, Destination = 0
	0x00, 0x01, 0x05, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	//	1bit  2bit  3bit  4bit  5bit  6bit  7bit  8bit  9bit, 10,   11,   12,   13,   14,   15,   16 
	0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B,
	//	2bit, 3bit, 3bit, 3bit, 3bit, 3bit, 4bit, 5bit, 6bit, 7bit, 8bit, 9bit
};

static const u08_t __jpeg_y_ac_dht[] = {
	0xFF, 0xC4, 0x00, 0xB5, 0x10, 0x00, 0x02, 0x01, 0x03, 0x03, 0x02, 0x04, 0x03, 0x05, 0x05, 0x04, //  1*16
	0x04, 0x00, 0x00, 0x01, 0x7D, 0x01, 0x02, 0x03, 0x00, 0x04, 0x11, 0x05, 0x12, 0x21, 0x31, 0x41, //  2*16
	0x06, 0x13, 0x51, 0x61, 0x07, 0x22, 0x71, 0x14, 0x32, 0x81, 0x91, 0xA1, 0x08, 0x23, 0x42, 0xB1, //  3*16
	0xC1, 0x15, 0x52, 0xD1, 0xF0, 0x24, 0x33, 0x62, 0x72, 0x82, 0x09, 0x0A, 0x16, 0x17, 0x18, 0x19, //  4*16
	0x1A, 0x25, 0x26, 0x27, 0x28, 0x29, 0x2A, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x3A, 0x43, 0x44, //  5*16
	0x45, 0x46, 0x47, 0x48, 0x49, 0x4A, 0x53, 0x54, 0x55, 0x56, 0x57, 0x58, 0x59, 0x5A, 0x63, 0x64, //  6*16
	0x65, 0x66, 0x67, 0x68, 0x69, 0x6A, 0x73, 0x74, 0x75, 0x76, 0x77, 0x78, 0x79, 0x7A, 0x83, 0x84, //  7*16
	0x85, 0x86, 0x87, 0x88, 0x89, 0x8A, 0x92, 0x93, 0x94, 0x95, 0x96, 0x97, 0x98, 0x99, 0x9A, 0xA2, //  8*16
	0xA3, 0xA4, 0xA5, 0xA6, 0xA7, 0xA8, 0xA9, 0xAA, 0xB2, 0xB3, 0xB4, 0xB5, 0xB6, 0xB7, 0xB8, 0xB9, //  9*16
	0xBA, 0xC2, 0xC3, 0xC4, 0xC5, 0xC6, 0xC7, 0xC8, 0xC9, 0xCA, 0xD2, 0xD3, 0xD4, 0xD5, 0xD6, 0xD7, //  10*16
	0xD8, 0xD9, 0xDA, 0xE1, 0xE2, 0xE3, 0xE4, 0xE5, 0xE6, 0xE7, 0xE8, 0xE9, 0xEA, 0xF1, 0xF2, 0xF3, // 11*16
	0xF4, 0xF5, 0xF6, 0xF7, 0xF8, 0xF9, 0xFA														// 11*16 + 7
};

static const u08_t __jpeg_y_qtable[64] = {
	2, 2, 2, 3, 2, 3, 4, 2,
	2, 4, 5, 4, 3, 4, 5, 6,
	5, 5, 5, 5, 6, 8, 7, 7,
	7, 7, 7, 8, 11, 9, 9, 9,
	9, 9, 9, 11, 11, 11, 11, 11,
	11, 11, 11, 12, 12, 12, 12, 12,
	12, 12, 12, 12, 12, 12, 12, 12,
	12, 12, 12, 12, 12, 12, 12, 12
};

static const u08_t __jpeg_header0[] = {
	0xFF, 0xD8,						// SOI
	0xFF, 0xE0,						// APP0
	0x00, 0x10,						// 16bytes
	0x4A, 0x46, 0x49, 0x46, 0x00,	// JFIF\0
	0x01, 0x01,						// version 1.01
	0x01,							// density units (dots per inch)
	0x00, 0x48,						// 72 dpi
	0x00, 0x48,						// 72 dpi
	0x00, 0x00,						// thumbnail 0 x 0
	0xFF, 0xDB,						// DQT
	0x00, 0x43,						// QT Length
	0x00							// Pq = 0 (8bit precision), Tq = 0 (id = 0)		
};

static const u08_t __jpeg_header1[] = {
	0xFF, 0xC0,						// SOF , baseline dct
	0x00, 0x0B,						// length 8 + 1*3
	0x08,							// 8 bits per sample
	0xFF, 0xFF, 					// Patch in Height here
	0xFF, 0xFF,						// Patch in Width here
	0x01,							// One channel

	0x01,							// Channel id 1
	0x11,							// 1:1 subsampling factor
	0x00 							// Use quantization table number 0
};

static const u08_t __jpeg_header2[] = {
	0xFF, 0xDA,						// SOS marker
	0x00, 0x08,						// Length
	0x01,							// Ns (=1)
	0x01,							// Csj = 1, 
	0x00,							// DC table = 0, AC table = 0
	0x00, 0x3F,						// Ss = 0, Se = 64
	0x00,							// Ah = 0, Al = 0
};


static void __jpeg_fdct(u08_t const *in, iint_t *ot) {
	iint_t x0, x1, x2, x3, x4, x5, x6, x7, i;
	for (i = 0; i < 64; i += 8) {

		x0 = in[i + 0] - in[i + 7];
		x1 = in[i + 6] - in[i + 1];
		x2 = in[i + 2] - in[i + 5];
		x3 = in[i + 3] - in[i + 4];
		x4 = in[i + 0] + in[i + 7];
		x5 = in[i + 1] + in[i + 6];
		x6 = in[i + 2] + in[i + 5];
		x7 = in[i + 3] + in[i + 4];

		ot[i + 1] = (H + C1*x0 - C3*x1 + C5*x2 + C7*x3) >> B;
		ot[i + 3] = (H + C3*x0 + C7*x1 - C1*x2 - C5*x3) >> B;
		ot[i + 5] = (H + C5*x0 + C1*x1 + C7*x2 + C3*x3) >> B;
		ot[i + 7] = (H + C7*x0 + C5*x1 + C3*x2 - C1*x3) >> B;

		x0 = x5 + x6;
		x1 = x5 - x6;
		x2 = x4 + x7;
		x3 = x4 - x7;

		ot[i + 0] = (H + C4*(x2 + x0 - 1024)) >> B;
		ot[i + 2] = (H + C2*x3 + C6*x1) >> B;
		ot[i + 4] = (H + C4*(x2 - x0)) >> B;
		ot[i + 6] = (H + C6*x3 - C2*x1) >> B;
	}

	for (i = 0; i < 8; ++i) {

		x0 = ot[0 + i] - ot[56 + i];
		x1 = ot[48 + i] - ot[8 + i];
		x2 = ot[16 + i] - ot[40 + i];
		x3 = ot[24 + i] - ot[32 + i];
		x4 = ot[0 + i] + ot[56 + i];
		x5 = ot[8 + i] + ot[48 + i];
		x6 = ot[16 + i] + ot[40 + i];
		x7 = ot[24 + i] + ot[32 + i];

		ot[8 + i] = (H + C1*x0 - C3*x1 + C5*x2 + C7*x3) >> B;
		ot[24 + i] = (H + C3*x0 + C7*x1 - C1*x2 - C5*x3) >> B;
		ot[40 + i] = (H + C5*x0 + C1*x1 + C7*x2 + C3*x3) >> B;
		ot[56 + i] = (H + C7*x0 + C5*x1 + C3*x2 - C1*x3) >> B;

		x0 = x5 + x6;
		x1 = x5 - x6;
		x2 = x4 + x7;
		x3 = x4 - x7;

		ot[0 + i] = (H + C4*(x2 + x0)) >> B;
		ot[16 + i] = (H + C2*x3 + C6*x1) >> B;
		ot[32 + i] = (H + C4*(x2 - x0)) >> B;
		ot[48 + i] = (H + C6*x3 - C2*x1) >> B;

	}
}

static void __jpeg_write(lifoda_jpeg_context_t *pContext, void const *pValue, int const size) {
	size_t bwritten = pContext->pWrite(pContext->pIOContext, pValue, size);
	if (bwritten != size)
		__jpeg_exit(pContext, LFD_JPEG_IO_ERROR);
	pContext->iBytesWritten += bwritten;
}

static void __jpeg_write_imm(lifoda_jpeg_context_t *pContext, u32_t val, int const size) {
	if (!size)
		return;
	switch (size) {
		case 1: break;
		case 2: val = __jpeg_u16(val & 0xffff); break;
		case 4: val = __jpeg_u32(val); break;
		default: __jpeg_exit(pContext, LFD_JPEG_IO_ERROR); break;
	}
	__jpeg_write(pContext, &val, size);
}


void __jpeg_write_byte(lifoda_jpeg_context_t *pContext, u08_t byte) {
	static u08_t const _zero = 0;
	if (pContext->pWrite(pContext->pIOContext, &byte, 1) < 1) {
		__jpeg_exit(pContext, LFD_JPEG_IO_ERROR);
		return;
	}

	if (byte != 0xff)
		return;

	if (pContext->pWrite(pContext->pIOContext, &_zero, 1) < 1) {
		__jpeg_exit(pContext, LFD_JPEG_IO_ERROR);
		return;
	}
}

void __jpeg_flush_bits(lifoda_jpeg_context_t *pContext) {
	u32_t flushbuff = 0;
	int len = 0;
	if (!pContext->iBuffLen)
		return;
	if (pContext->iBuffLen > 31) {
		__jpeg_write_byte(pContext, (pContext->iBuff >> 0x18) & 0xff);
		__jpeg_write_byte(pContext, (pContext->iBuff >> 0x10) & 0xff);
		__jpeg_write_byte(pContext, (pContext->iBuff >> 0x08) & 0xff);
		__jpeg_write_byte(pContext, (pContext->iBuff >> 0x00) & 0xff);
		pContext->iBuff = 0;
		pContext->iBuffLen = 0;
		return;
	}
	len = (pContext->iBuffLen + 7) >> 3;
	flushbuff = (pContext->iBuff << (32 - pContext->iBuffLen)) | (0xffffffff >> pContext->iBuffLen);
	flushbuff = __jpeg_u32(flushbuff);
	while (len > 0) {
		__jpeg_write_byte(pContext, flushbuff & 0xff);
		flushbuff >>= 8;
		--len;
	}
	pContext->iBuff = 0;
	pContext->iBuffLen = 0;
}

void __jpeg_write_bits(lifoda_jpeg_context_t *pContext, u32_t bits, int const count, int const flush) {

	int rem = 32 - pContext->iBuffLen;
	bits = bits & ((1 << count) - 1);
	if (count <= rem) {
		pContext->iBuff = pContext->iBuff | (bits << (rem - count));
		pContext->iBuffLen += count;
		if (pContext->iBuffLen > 31 || flush)
			__jpeg_flush_bits(pContext);
		return;
	}
	rem = count - rem;
	pContext->iBuff = pContext->iBuff | (bits >> rem);
	pContext->iBuffLen = 32;
	__jpeg_flush_bits(pContext);
	pContext->iBuff = bits << (32 - rem);
	pContext->iBuffLen = rem;
	if (pContext->iBuffLen > 31 || flush)
		__jpeg_flush_bits(pContext);

}

void __jpeg_prepare_mcu(lifoda_jpeg_context_t *pContext, int *dc, int const *pBuffer, int *pOutput, u08_t const *pQTable) {
	int i, j, h;

	h = pBuffer[0] / pQTable[0];
	pOutput[0] = h - *dc;
	*dc = h;

	for (i = 1; i < 64; ++i) {
		j = __jpeg_fzigzag[i];
		pOutput[j] = pBuffer[i] / pQTable[j];
	}
}

void __jpeg_encode_value(lifoda_jpeg_context_t *pContext, u08_t const prezero, int const value, u08_t const *pLengths, u16_t const *pCodes) {
	u32_t uvalue = 0;
	u08_t bits = 0;
	u08_t code = 0;
	int length = 0;
	bits = __jpeg_log2(value) & 0xf;
	code = (prezero << 4) | bits;
	uvalue = pCodes[code];
	length = pLengths[code];
	uvalue = uvalue << bits;
	uvalue = uvalue | ((value < 0 ? value - 1 : value) & ((1 << bits) - 1));
	__jpeg_write_bits(pContext, uvalue, bits + length, 0);
}

void __jpeg_encode_mcu(lifoda_jpeg_context_t *pContext, int const *ZZ) {
	int k = 0, r = 0;

	__jpeg_encode_value(pContext, 0, ZZ[0], __jpeg_y_dc_size, __jpeg_y_dc_code);
	for (k = 1;; ++k) {
		if (ZZ[k]) {
			for (; r > 15; r -= 16)
				__jpeg_encode_value(pContext, 15, 0, __jpeg_y_ac_size, __jpeg_y_ac_code);
			__jpeg_encode_value(pContext, r, ZZ[k], __jpeg_y_ac_size, __jpeg_y_ac_code);
			r = 0;
			if (k < 63)
				continue;
			break;
		}
		if (k < 63) {
			++r;
			continue;
		}
		__jpeg_encode_value(pContext, 0, 0, __jpeg_y_ac_size, __jpeg_y_ac_code);
		break;
	}
}

void __jpeg_encode_image(lifoda_jpeg_context_t *pContext, lifoda_jpeg_rect_t const *pRect) {
	int *pBuffer;
	int *pOutput;
	u08_t *pSample;
	int bx, by, x, y, dc;
	lifoda_jpeg_rect_t rect;
	bx = (pRect->w + 7) & ~7;
	by = (pRect->h + 7) & ~7;
	dc = 0;

	pSample = __jpeg_alloc(pContext, 64 * sizeof(*pSample));
	pBuffer = __jpeg_alloc(pContext, 64 * sizeof(*pBuffer));
	pOutput = __jpeg_alloc(pContext, 64 * sizeof(*pOutput));
	for (y = 0; y < by; y += 8) {
		for (x = 0; x < bx; x += 8) {
			rect.x = x;
			rect.y = y;
			rect.w = 8;
			rect.h = 8;
			pContext->pChunkFetch(pContext->pIMContext, pSample, &rect, 0);
			__jpeg_fdct(pSample, pBuffer);
			__jpeg_prepare_mcu(pContext, &dc, pBuffer, pOutput, __jpeg_y_qtable);
			__jpeg_encode_mcu(pContext, pOutput);
		}
	}
	__jpeg_flush_bits(pContext);
	return;
}


int lifoda_jpeg_encode(lifoda_jpeg_context_t *pContext, void *pInContext, void *pOutContext) {
	lifoda_jpeg_rect_t rect;

	pContext->pIOContext = pOutContext;
	pContext->pIMContext = pInContext;

	if (setjmp(pContext->jbLongJump)) {
		return pContext->iStatus;
	}
	pContext->pChunkFetch(pContext->pIMContext, NULL, &rect, 0);
	__jpeg_write(pContext, __jpeg_header0, sizeof(__jpeg_header0));
	__jpeg_write(pContext, __jpeg_y_qtable, sizeof(__jpeg_y_qtable));
	__jpeg_write(pContext, __jpeg_header1, 5);
	__jpeg_write_imm(pContext, (rect.h << 16) | rect.w, 4);
	__jpeg_write(pContext, __jpeg_header1 + 9, sizeof(__jpeg_header1) - 9);
	__jpeg_write(pContext, __jpeg_y_dc_dht, sizeof(__jpeg_y_dc_dht));
	__jpeg_write(pContext, __jpeg_y_ac_dht, sizeof(__jpeg_y_ac_dht));
	__jpeg_write(pContext, __jpeg_header2, sizeof(__jpeg_header2));
	__jpeg_encode_image(pContext, &rect);
	__jpeg_write_imm(pContext, 0xFFD9, 2); // EOI marker
	return LFD_JPEG_SUCCESS;
}


int lifoda_jpeg_init_encoder_context(
	lifoda_jpeg_context_t *pContext,			// jpeg encode/decode context
	lifoda_jpeg_write_t	*pWrite,				// encoded stream write function
	lifoda_jpeg_chunk_fetch_t *pChunkFetch,		// bitmap reading function
	void *pHeapStart,							// for jpeg working data
	size_t iHeapSize,							// size of this memory (note: if allocated too little ram , it can run out of it and abort)	
	u32_t flags) 								// reserved for flags, atm 0
{
	if (!pContext || !pWrite || !pChunkFetch)
		return LFD_JPEG_INVALID_PARAMS;
	if (!iHeapSize && pHeapStart)
		return LFD_JPEG_INVALID_PARAMS;
	memset(pContext, 0, sizeof(*pContext));
	pContext->iFlags = flags;
	if (!pHeapStart || flags & LFD_JPEG_FLAGS_USE_MALLOC) {
		iHeapSize = iHeapSize ? iHeapSize : (1024 * 256);
		pHeapStart = malloc(iHeapSize);
		pContext->iFlags |= LFD_JPEG_FLAGS_USE_MALLOC;
	}
	//memset (pHeapStart, 0, iHeapSize);	
	pContext->pWrite = pWrite;
	pContext->pChunkFetch = pChunkFetch;
	pContext->pHeapStart = pHeapStart;
	pContext->iHeapSize = iHeapSize;
	pContext->iHeapUsed = 0;
	return LFD_JPEG_SUCCESS;
}

#endif