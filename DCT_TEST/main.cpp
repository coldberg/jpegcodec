#include <SDL.h>
#include "lifoda_jpeg.h"
#include <cstdlib>
#include <cmath>
#include <cstdio>
#include <cassert>
#include <cstring>

#undef main										
												
#define MY_WINDOW_WIDTH		1280				
#define MY_WINDOW_HEIGHT	768			
#define MY_WINDOW_TITLE		"SDL_TEMPLATE"
#define MY_WINDOW_FLAGS		0

struct Context {
	SDL_Surface *toDrawTo;
	SDL_Window *pWindow;
	int w,h;
	size_t iSize;
	size_t iIndex;
	u08_t *pData;
};

int _draw (Context *pContext, void const *pData, lifoda_jpeg_rect_t const *pRect, int flags) {
	
	if (!pData) {
		pContext->w = pRect->w;
		pContext->h = pRect->h;
		return 1;
	}

	u32_t *pixels = (u32_t *)pContext->toDrawTo->pixels;
	u32_t ww = pContext->toDrawTo->w;
	u32_t hh = pContext->toDrawTo->h;
	int xx, yy;
	u08_t r, g, b;

	for (unsigned y = 0; y < pRect->h; ++y) {
		for (unsigned x = 0; x < pRect->w; ++x) {
			xx = pRect->x + x;
			yy = pRect->y + y;			
			r = ((u08_t *)pData) [(x + y * pRect->w) * 3 + 0];
			g = ((u08_t *)pData) [(x + y * pRect->w) * 3 + 1];
			b = ((u08_t *)pData) [(x + y * pRect->w) * 3 + 2];				
			pixels [xx + yy * ww] = (r << 16) | (g << 8) | b;
		}
	}
	return 1;
}

int _read (Context *pContext, void const **pData, int size) {	
	if ((size = SDL_min ((int)(pContext->iSize - pContext->iIndex), size)) < 1)
		return 0;	
	if (pData) *pData = &pContext->pData [pContext->iIndex];
	pContext->iIndex += size;	
	return size;
}

int main (int, char **) {
//	freopen ("DATA/log.txt", "w", stdout);

	Context draw_context;

//  LOAD FILE
	FILE *pFile = fopen  ("DATA/test0b.jpg", "rb");
	assert (pFile != NULL);

	fseek (pFile, 0, SEEK_END);
	size_t sz = ftell (pFile);
	fseek (pFile, 0, SEEK_SET);

	draw_context.pData = (u08_t *)malloc (sz);
	draw_context.iSize = sz;
	draw_context.iIndex = 0;

	fread (draw_context.pData, 1, sz, pFile);

	fclose (pFile);



	size_t iDecodeBufferSize = 256*1024;

	void *pDecodeBuffer = malloc (iDecodeBufferSize);
	memset (pDecodeBuffer, 0, iDecodeBufferSize);
	
//  END LOAD FILE


	SDL_Init (SDL_INIT_EVERYTHING);				
	atexit (SDL_Quit);							
	SDL_GL_SetAttribute							
		(SDL_GL_DOUBLEBUFFER, 1);				
												
	SDL_Window *pWindow = SDL_CreateWindow (
		MY_WINDOW_TITLE,
		SDL_WINDOWPOS_CENTERED,					
		SDL_WINDOWPOS_CENTERED,					
		MY_WINDOW_WIDTH,						
		MY_WINDOW_HEIGHT,						
		MY_WINDOW_FLAGS);

	SDL_Surface *pSurface = 
		SDL_GetWindowSurface (pWindow);

	draw_context.pWindow = pWindow;
	draw_context.toDrawTo = SDL_GetWindowSurface (pWindow);

	SDL_Event sEvent;

	u64_t frames = 0;
	u64_t time0, time1;
	double time = 0.0;
	

	for (;;) {	
		
		if (SDL_PollEvent (&sEvent)) {			
			if (sEvent.type == SDL_QUIT) {		
				break;
			}
			continue;
		}

		SDL_LockSurface (draw_context.toDrawTo);		
		time0 = SDL_GetPerformanceCounter (); 
///////////////////////////////////
		lifoda_jpeg_decode (
			(lifoda_jpeg_read_t *)&_read,
			&draw_context,
			(lifoda_jpeg_patch_write_t *)&_draw,
			&draw_context,
			pDecodeBuffer,
			iDecodeBufferSize,
			0);
////////////////////////////////
		time1 = SDL_GetPerformanceCounter();
		++frames;
		time += (1000.0*(time1 - time0)) / SDL_GetPerformanceFrequency();

		draw_context.iIndex = 0;

		SDL_UnlockSurface (draw_context.toDrawTo);
		SDL_UpdateWindowSurface (pWindow);
		if (frames & 0x7F) {
			continue;
		}
		
		printf("%lf ms / %llu decodes = %lf ms per decode\r", time, frames, (time / frames));
		frames = 0;
		time = 0.0;
	}
			
	SDL_DestroyWindow (pWindow);

	free (draw_context.pData);
	
	return 0;
} 
